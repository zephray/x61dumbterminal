Mod ThinkPad X61 into a Type-C keyboard/video/mouse console (like a dumb terminal in the current age) that can be used with SBCs or other handheld devices.

# Features

- USB Type-C display input
- USB 2.0 Hub
- USB Keyboard and Trackpoint
- Battery charger supporting original battery
- PD output up to 5V5A / 9V4A / 12V4A

It however, does not support HDD/ audio/ ethernet/ modem/ external video output/ dock so these ports are left unused and can/ should be covered up.

# License

The hardware design is released under the CERN Open Source Hardware License permissive variant, CERN-OHL-P. A copy of the license is provided in the source repository. Additionally, user guide of the license is provided on ohwr.org.
